package org.acme;


import Data.DDRComponentData;
import io.agroal.api.AgroalDataSource;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Path("/components")
public class DDRComponents {
    @Inject
    AgroalDataSource bankAlHabibDataSource;
    static Connection conn=null;

    DDRComponentData compData;

    // @GET  // Show
    // @POSt  // Insert
    // @PUT  //  Update
    // @DELETE  // Remove

    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRComponentData> DataGet() throws Exception {
        CreateConnection();
        try {
            Statement statement = conn.createStatement();

          /*  JSONObject obj= new JSONObject(dataGet);
              String component_name = obj.optString("component_name");
          */
            List<DDRComponentData> list=  new ArrayList<>();

            ResultSet rs = statement.executeQuery("Select * from components;"); // where component_name ="+ component_name +";");

            while (rs.next()) {
                compData = new DDRComponentData();
                compData.comp_id = rs.getString("comp_id"); //
                compData.component_id = rs.getString("component_id");
                compData.component_name = rs.getString("component_name");
                compData.type = rs.getString("type");
                compData.url = rs.getString("url");
                compData.default_value = rs.getString("default_value");
                compData.validation_set = rs.getString("validation_set");
                compData.event_set = rs.getString("event_set");

                list.add(compData);
                System.out.println("Values to show :" + compData.component_id + "  ----------  " + compData.component_name);
            }


            conn.close();
            return  list;

          /*  String string = "{\"name\": \"Sam Smith\", \"technology\": \"Python\"}";
            JSONObject json = new JSONObject(string);

            return  json;*/

          //  return  "BBBBB";
        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return null;
        }

    }

    @Path("/getcomponent")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRComponentData> DataGetComponent(String dataGet) throws Exception {
        CreateConnection();
        try {
            Statement statement = conn.createStatement();

            JSONObject obj= new JSONObject(dataGet);
            String component_id = obj.optString("comp_id");
            List<DDRComponentData> list=  new ArrayList<>();

            ResultSet rs = statement.executeQuery("Select * from components where comp_id="+" '"  +component_id +"'"); // where component_name ="+ component_name +";");

            while (rs.next()) {
                compData = new DDRComponentData();
                compData.component_id = rs.getString("component_id");
                compData.component_name = rs.getString("component_name");
                compData.type = rs.getString("type");
                compData.url = rs.getString("url");
                compData.default_value = rs.getString("default_value");
                compData.validation_set = rs.getString("validation_set");
                compData.event_set = rs.getString("event_set");

                list.add(compData);
                System.out.println("Values to show :" + compData.component_id + "  ----------  " + compData.component_name);
            }


            conn.close();
            return  list;

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return null;
        }
    }

    @Path("/insert")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataInsert(String dataGet) throws Exception {
        CreateConnection();
        try {
            compData = new DDRComponentData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            compData.component_id = obj.optString("component_id");

            compData.component_name =  obj.optString("component_name");
            compData.type =  obj.optString("type");
            compData.url =  obj.optString("url");
            compData.default_value =  obj.optString("default_value");
            compData.validation_set =  obj.optString("validation_set");
            compData.event_set =  obj.optString("event_set");

            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into components (component_id, component_name, type, " +
                    "url, default_value, validation_set, event_set) values ( '" + compData.component_id + "' , '" +
                    compData.component_name +"' , '" +compData.type +"' , '" +compData.url +"' , '" +compData.default_value +"' , '" +
                    compData.validation_set +"' , '" +compData.event_set +"' );");

            conn.close();
        }catch (Exception e){
            System.out.println("Exception Occurred :"+ e);

            conn.close();
            e.printStackTrace();
        }
    }

    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataUpdate(String dataGet) throws Exception {
        CreateConnection();
        try {
            compData = new DDRComponentData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);

            compData.comp_id = obj.optString("comp_id");
            compData.component_id = obj.optString("component_id");

            compData.component_name =  obj.optString("component_name");
            compData.type =  obj.optString("type");
            compData.url =  obj.optString("url");
            compData.default_value =  obj.optString("default_value");
            compData.validation_set =  obj.optString("validation_set");
            compData.event_set =  obj.optString("event_set");

            Statement statement = conn.createStatement();
            statement.executeUpdate("update components SET component_id='"+compData.component_id +"'," +
                    " component_name='"+compData.component_name +"', type='"+compData.type +"', url='" +compData.url +"', " +
                    "default_value='" +compData.default_value +"', validation_set='" +compData.validation_set +"', " +
                    "event_set='" +compData.event_set +"'  where comp_id = " + "'"+ compData.comp_id  + "'" );
         //   conn.commit();
            conn.close();

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
        }
    }

    //Deleting Data inside Database
    @Path("/delete")
    @POST
    public void DataDelete(String dataGet) throws Exception {
        CreateConnection();
        try {

            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            String componentData = obj.optString("comp_id");

            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM components WHERE comp_id = '"+componentData+"';");

            System.out.println("Comp ID :" + componentData);

            conn.close();
        }catch (Exception e){
            conn.close();
            e.printStackTrace();
        }
    }

    public void CreateConnection() {
        try {
            try {
                conn  = bankAlHabibDataSource.getConnection();
            } catch (Exception e) {
                System.out.println("Error in connection" + e);
            }
        }catch (Exception e){
            System.out.println("Error in connection" + e);
        }
    }


}

        /*

        "component_name": "251",
        "type": "n1",
        "url": "ki",
        "default_value": "87fd://",
        "validation_set": "8" ,
        "event_set": "cftory"

          */
