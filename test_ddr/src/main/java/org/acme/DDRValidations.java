package org.acme;

import Data.DDRValidationData;
import io.agroal.api.AgroalDataSource;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Path("/validations")
public class DDRValidations {
    @Inject
    AgroalDataSource bankAlHabibDataSource;
    static Connection conn=null;


    DDRValidationData valData;

    // @GET  // Show
    // @POSt  // Insert
    // @PUT  //  Update
    // @DELETE  // Remove

    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRValidationData> DataGet() throws Exception {
        CreateConnection();
        try {
            Statement statement = conn.createStatement();
          /*  JSONObject obj= new JSONObject(dataGet);
            String component_name = obj.optString("component_name");
           */
            List<DDRValidationData> list=  new ArrayList<>();

            ResultSet rs = statement.executeQuery("Select * from validations;"); // where component_name ="+ component_name +";");
            while (rs.next()) {
                valData = new DDRValidationData();
                valData.validation_id = rs.getString("validation_id");
                valData.validation_name = rs.getString("validation_name");
                valData.type = rs.getString("type");
                valData.checks = rs.getString("checks");
                valData.comments = rs.getString("comments");

                list.add(valData);
                System.out.println("Values to show :" + valData.validation_id + "  ----------  " + valData.validation_name);
            }

            conn.close();
            return  list;

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return null;
        }
    }

    @Path("/getvalidation")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRValidationData> DataGetValidation(String dataGet) throws Exception {
        CreateConnection();
        try {
            Statement statement = conn.createStatement();

            JSONObject obj= new JSONObject(dataGet);
            String validation_id = obj.optString("validation_id");
            List<DDRValidationData> list=  new ArrayList<>();

            ResultSet rs = statement.executeQuery("Select * from validations where validation_id="+" '"  +validation_id +"'"); // where component_name ="+ component_name +";");
            while (rs.next()) {
                valData = new DDRValidationData();
                valData.validation_id = rs.getString("validation_id");
                valData.validation_name = rs.getString("validation_name");
                valData.type = rs.getString("type");
                valData.checks = rs.getString("checks");
                valData.comments = rs.getString("comments");

                list.add(valData);
                System.out.println("Values to show :" + valData.validation_id + "  ----------  " + valData.validation_name);
            }

            conn.close();
            return  list;

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return null;
        }

    }

    @Path("/insert")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataInsert(String dataGet) throws Exception {
        CreateConnection();
        try {
            valData = new DDRValidationData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            valData.validation_id = obj.optString("validation_id");
            valData.validation_name =  obj.optString("validation_name");
            valData.type =  obj.optString("type");
            valData.checks =  obj.optString("checks");
            valData.comments =  obj.optString("comments");

            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into validations (validation_id, validation_name, type, " +
                    "checks, comments) values ( '" + valData.validation_id + "' , '" +
                    valData.validation_name +"' , '" +valData.type +"' , '" +valData.checks +"' , '" +valData.comments +"' );");

            conn.close();
        }catch (Exception e){
            System.out.println("Exception Occurred :"+ e);
            conn.close();
            e.printStackTrace();
        }
    }

    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataUpdate(String dataGet) throws Exception {
        CreateConnection();
        try {
            valData = new DDRValidationData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            valData.validation_id = obj.optString("validation_id");
            valData.validation_name =  obj.optString("validation_name");
            valData.type =  obj.optString("type");
            valData.checks =  obj.optString("checks");
            valData.comments =  obj.optString("comments");

            Statement statement = conn.createStatement();
            statement.executeUpdate("update validations SET validation_id='"+valData.validation_id +"'," +
                    " validation_name='"+valData.validation_name +"', type='"+valData.type +"', checks='" +valData.checks +"', " +
                    "comments='" +valData.comments +"'  where validation_id = " + "'"+ valData.validation_id  + "'" );
            //   conn.commit();
            conn.close();

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
        }
    }

    //Deleting Data inside Database
    @Path("/delete")
    @POST
    public void DataDelete(String dataGet) throws Exception {
        CreateConnection();
        try {

            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            String validationData = obj.optString("delete_id");

            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM validations WHERE validation_id = '"+validationData+"';");

            System.out.println("validation ID :" + validationData);
            conn.close();
        }catch (Exception e){
            conn.close();
            e.printStackTrace();
        }
    }


    public void CreateConnection() {
        try {
            try {
                conn  = bankAlHabibDataSource.getConnection();
            } catch (Exception e) {
                System.out.println("Error in connection" + e);
            }
        }catch (Exception e){
            System.out.println("Error in connection" + e);
        }
    }


}

/*
{
        "validation_id": "13",
        "validation_name":"23dd",
        "type":"afa",
        "checks":"n76jsd",
        "comments":"8qwe"

        }*/
