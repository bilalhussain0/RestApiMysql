package org.acme;

import Data.DDRScreenData;
import io.agroal.api.AgroalDataSource;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Path("/screens")
public class DDRScreens {
    @Inject
    AgroalDataSource bankAlHabibDataSource;
    static Connection conn=null;

    DDRScreenData screenData;

    // @GET  // Show
    // @POST  // Insert
    // @PUT  //  Update
    // @DELETE  // Remove

    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRScreenData> DataGet() throws Exception {
        CreateConnection();
        try {

          /* JSONObject obj= new JSONObject(dataGet);
            String screen_name = obj.optString("screen_name");*/

            List<DDRScreenData> list=  new ArrayList<>();

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("Select * from screens;");// where screen_name= '"+ screen_name +"'");

            while (rs.next()) {
                screenData= new DDRScreenData();

                screenData.Package = rs.getString("Package");
                screenData.Class =  rs.getString("Class");
                screenData.screen_name =  rs.getString("screen_name");
                screenData.set_of_screen_title =  rs.getString("set_of_screen_title");
                screenData.urd_screen_id =  rs.getString("urd_screen_id");
                screenData.status_bar_c =  rs.getString("status_bar_c");
                screenData.status_bar_l =  rs.getString("status_bar_l");
                screenData.status_bar_r =  rs.getString("status_bar_r");

                list.add(screenData);

              // System.out.println("Values to show :" + screenData.Package + "  ----------  " + screenData.screen_name);
            }

            conn.close();
            return list;
         //   return  objReturn;

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return  null;
        }
    }

    @Path("/getscreen")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<DDRScreenData> DataGetScreen(String dataGet) throws Exception {
        CreateConnection();
        try {
            Statement statement = conn.createStatement();

            JSONObject obj= new JSONObject(dataGet);
            String screen_id = obj.optString("urd_screen_id");
            List<DDRScreenData> list=  new ArrayList<>();

            ResultSet rs = statement.executeQuery("Select * from screens where urd_screen_id= '"+ screen_id +"'");
            while (rs.next()) {
                screenData= new DDRScreenData();

                screenData.Package = rs.getString("Package");
                screenData.Class =  rs.getString("Class");
                screenData.screen_name =  rs.getString("screen_name");
                screenData.set_of_screen_title =  rs.getString("set_of_screen_title");
                screenData.urd_screen_id =  rs.getString("urd_screen_id");
                screenData.status_bar_c =  rs.getString("status_bar_c");
                screenData.status_bar_l =  rs.getString("status_bar_l");
                screenData.status_bar_r =  rs.getString("status_bar_r");

                list.add(screenData);

                //  System.out.println("Values to show :" + screenData.Package + "  ----------  " + screenData.screen_name);
            }

            conn.close();
            return  list;

        }catch (Exception e){
            conn.close();
            e.printStackTrace();
            return null;
        }

    }


    @Path("/insert")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataInsert(String dataGet) throws SQLException {
        CreateConnection();
        try {
            screenData= new DDRScreenData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            screenData.Package = obj.optString("Package");
            screenData.Class =  obj.optString("Class");
            screenData.screen_name =  obj.optString("screen_name");
            screenData.set_of_screen_title =  obj.optString("set_of_screen_title");
            screenData.urd_screen_id =  obj.optString("urd_screen_id");
            screenData.status_bar_c =  obj.optString("status_bar_c");
            screenData.status_bar_l =  obj.optString("status_bar_l");
            screenData.status_bar_r =  obj.optString("status_bar_r");

            Statement statement = conn.createStatement();
            statement.executeUpdate("insert into screens (Package, Class, screen_name, " +
                    "set_of_screen_title, urd_screen_id, status_bar_c, status_bar_l, status_bar_r) values ( '" + screenData.Package + "' , '" +
                    screenData.Class +"' , '" +screenData.screen_name +"' , '" +screenData.set_of_screen_title +"' , '" +screenData.urd_screen_id +"' , '" +
                    screenData.status_bar_c +"' , '" +screenData.status_bar_l  +"' , '"+ screenData.status_bar_r +"' );");

            conn.close();
        }catch (Exception e){

            conn.close();
            e.printStackTrace();
        }
    }


    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public void DataUpdate(String dataGet) throws Exception {
        CreateConnection();
        try {
            screenData= new DDRScreenData();
            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            screenData.Package = obj.optString("Package");
            screenData.Class =  obj.optString("Class");
            screenData.urd_screen_id =  obj.optString("urd_screen_id");
            screenData.screen_name =  obj.optString("screen_name");
            screenData.set_of_screen_title =  obj.optString("set_of_screen_title");
            screenData.status_bar_l =  obj.optString("status_bar_l");
            screenData.status_bar_c =  obj.optString("status_bar_c");
            screenData.status_bar_r =  obj.optString("status_bar_r");

            Statement statement = conn.createStatement();
            statement.executeUpdate("update screens SET Package='"+screenData.Package +"'," +
                    " Class='"+screenData.Class +"', urd_screen_id='"+screenData.urd_screen_id +"', screen_name='" + screenData.screen_name +"', " +
                    "set_of_screen_title='" +screenData.set_of_screen_title +"', status_bar_l='"+screenData.status_bar_l +"', status_bar_c='"+screenData.status_bar_c +"', status_bar_r='"+screenData.status_bar_r +"'  where urd_screen_id = " + "'"+ screenData.urd_screen_id  + "'" );
            //   conn.commit();
            conn.close();

        }catch (Exception e){
            conn.rollback();
            conn.close();
            e.printStackTrace();
        }
    }

    //Deleting Data inside Database
    @Path("/delete")
    @POST
    public void DataDelete(String dataGet) throws Exception {
        CreateConnection();
        try {

            System.out.println("Json Get :" + dataGet);

            JSONObject obj= new JSONObject(dataGet);
            String validationData = obj.optString("delete_id");

            conn.setAutoCommit(false);
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM screens WHERE urd_screen_id = '"+validationData+"';");

            System.out.println("urd Screen ID :" + validationData);
            conn.close();
        }catch (Exception e){
            conn.rollback();
            conn.close();
            e.printStackTrace();
        }
    }

    public void CreateConnection() {
        try {
            try {
                conn  = bankAlHabibDataSource.getConnection();
            } catch (Exception e) {
                System.out.println("Error in connection" + e);
            }
        }catch (Exception e){
            System.out.println("Error in connection" + e);
        }
    }
}

/*

        "Package": "251",
        "Class": "n1",
        "urd_screen_id": "ki",
        "screen_name": "87fd://",
        "set_of_screen_title": "8" ,
        "status_bar_l": "cftory",
        "status_bar_c": "d",
        "status_bar_r": "ntsd"







*/


