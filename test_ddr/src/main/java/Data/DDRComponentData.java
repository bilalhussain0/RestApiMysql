package Data;

public class DDRComponentData {

    public String comp_id;
    public String component_id;
    public String component_name;
    public String type;
    public String url;
    public String default_value;
    public String validation_set;
    public String event_set;

}
