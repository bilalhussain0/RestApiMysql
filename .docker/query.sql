CREATE DATABASE IF NOT EXISTS ddr;
use ddr;
CREATE TABLE components(
	comp_id INT NOT NULL AUTO_INCREMENT,
	component_id VARCHAR(2000),
	component_name VARCHAR(2000),
	type VARCHAR(2000),
	url VARCHAR(2000),
	default_value VARCHAR(2000),
	validation_set VARCHAR(2000),
	event_set VARCHAR(2000),
	PRIMARY KEY (comp_id)
);
CREATE TABLE validations(
	val_id INT NOT NULL AUTO_INCREMENT,
	validation_id VARCHAR(2000),
	validation_name VARCHAR(2000),
	type VARCHAR(2000),
	checks VARCHAR(2000),
	comments VARCHAR(2000),
	PRIMARY KEY (val_id)
);  
CREATE TABLE screens(
	scrn_id INT NOT NULL AUTO_INCREMENT,
	package VARCHAR(2000),
	class VARCHAR(2000),
	urd_screen_id VARCHAR(2000),
	screen_name VARCHAR(2000),
	set_of_screen_title VARCHAR(2000),
	status_bar_l VARCHAR(2000),
	status_bar_c VARCHAR(2000),
	status_bar_r VARCHAR(2000),
	PRIMARY KEY (scrn_id)
);

